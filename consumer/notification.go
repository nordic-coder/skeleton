package consumer

import (
	"log"
	"sync"
)

var (
	instance *Notification
	once     sync.Once
)

type Notification struct{}

func NewNotification() *Notification {
	once.Do(func() {
		log.Println("new notification")
		instance = &Notification{}
	})
	return instance
}

func (Notification) Run() (err error) {
	// log.Println("hello")
	return
}
