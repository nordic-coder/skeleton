package handler

import (
	"net/http"
	"log"

	"github.com/labstack/echo/v4"
	"gitlab.ghn.vn/online/common/config"
	validator "gopkg.in/go-playground/validator.v9"
)

var cfg = config.GetConfig()

// NewValidator ..
func NewValidator() *MyValidator {
	return &MyValidator{validator: validator.New()}
}

// MyValidator ..
type MyValidator struct {
	validator *validator.Validate
}

// Validate ..
func (cv *MyValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}
// Error func
func Error(err error, c echo.Context) {
	code := http.StatusBadRequest
	msg := http.StatusText(code)

	if httpError, ok := err.(*echo.HTTPError); ok {
		code = httpError.Code
		msg = httpError.Message.(string)
	}

	if cfg.Debug {
		msg = err.Error()
	}

	log.Println(err)

	if !c.Response().Committed {
		if c.Request().Method == echo.HEAD {
			c.NoContent(code)
		} else {
			c.JSON(code, &ResponseContent{Code: code, Message: msg})
		}
	}
}

func getRequestID(c echo.Context) (requestID string) {
	if c.Get("reqID") != nil {
		requestID = c.Get("reqID").(string)
	}
	return
}

// ResponseContent struct
type ResponseContent struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func success(data interface{}) (int, ResponseContent) {
	return http.StatusOK, ResponseContent{
		Code:    http.StatusOK,
		Message: "Success",
		Data:    data,
	}
}
