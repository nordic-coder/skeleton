package handler

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/nordic-coder/skeleton/model"
	auditRepo "gitlab.com/nordic-coder/skeleton/repo/audit"
)

// CallLogs handler
var CallLogs callLogsHandler

func init() {
	CallLogs = callLogsHandler{}
}

type callLogsHandler struct {
}

func (o callLogsHandler) getSystemName() string {
	return "auditlog"
}

func (o callLogsHandler) getTypeName() string {
	return "call-log"
}

// Create func
func (o callLogsHandler) Create(c echo.Context) (err error) {

	type myRequest struct {
		OrderCode    string `json:"order_code" query:"order_code" validate:"required"`
		Duration     int    `json:"duration"  query:"duration" validate:"required"`
		EmployeeID   int    `json:"employee_id"  query:"employee_id" validate:"required"`
		PhoneCall    string `json:"phone_call" query:"phone_call" validate:"required"`
		PhoneReceive string `json:"phone_receive" query:"phone_receive" validate:"required"`
	}

	request := new(myRequest)
	if err = c.Bind(request); err != nil {
		return
	}
	if err = c.Validate(request); err != nil {
		return
	}

	audit := model.Audit{
		Type:       "call-log",
		Identity:   request.OrderCode,
		Action:     "call",
		EmployeeID: request.EmployeeID,
		Info: map[string]interface{}{
			"duration":      request.Duration,
			"phone_call":    request.PhoneCall,
			"phone_receive": request.PhoneReceive,
		},
	}

	err = auditRepo.New().Create(audit)
	if err != nil {
		return
	}

	return c.JSON(success(audit))
}

// Get func
func (o callLogsHandler) Get(c echo.Context) (err error) {

	type myRequest struct {
		OrderCode string `json:"order_code" query:"order_code" validate:"required"`
		Offset    int    `json:"offset" query:"offset"`
	}

	request := new(myRequest)
	if err = c.Bind(request); err != nil {
		return
	}
	if err = c.Validate(request); err != nil {
		return
	}

	result, err := auditRepo.New().AllCallLog(request.OrderCode, request.Offset)
	if err != nil {
		return
	}

	return c.JSON(success(result))
}
