package web

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/nordic-coder/skeleton/web/route"
	"gitlab.com/nordic-coder/skeleton/web/handler"
)

// Skeleton ..
var Skeleton gSkeleton

// gSkeleton consumer
type gSkeleton struct{}

func (gSkeleton) Start() error {

	// Init db
	// cfg.Mongo.Get("skeleton").Init()

	// Echo instance
	e := echo.New()
	e.Validator = handler.NewValidator()
	e.HTTPErrorHandler = handler.Error

	// Middlewares
	e.Use(middleware.RequestID())
	e.Use(middleware.Recover())
	e.Use(middleware.Secure())

	// Routes => handler
	route.Skeleton(e)

	// Start server
	go func() {
		if err := e.Start(":" + cfg.Port["skeleton"]); err != nil {
			log.Println("⇛ shutting down the server")
			log.Printf("%v\n", err.Error())
		}
	}()

	// Graceful Shutdown
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGTERM)
	signal.Notify(quit, syscall.SIGINT)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}

	return nil
}
