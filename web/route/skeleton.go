package route

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/nordic-coder/skeleton/web/handler"
)

// Skeleton route
func Skeleton(e *echo.Echo) {
	apiRoute := e.Group("/api")

	// call-log
	apiRoute.Any("/call-logs/create", handler.CallLogs.Create)
	apiRoute.Any("/call-logs", handler.CallLogs.Get)

}
