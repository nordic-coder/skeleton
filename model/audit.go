package model

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

// Audit struct
type Audit struct {
	ID          bson.ObjectId `json:"ID,omitempty" bson:"_id,omitempty"`
	Type        string        `json:"type,omitempty" bson:"type,omitempty"`
	Identity    string        `json:"identity,omitempty" bson:"identity,omitempty"`
	Action      string        `json:"action,omitempty" bson:"action,omitempty"`
	Description string        `json:"description,omitempty" bson:"description,omitempty"`
	EmployeeID  int           `json:"employee_id,omitempty" bson:"employee_id,omitempty"`
	ClientID    int           `json:"client_id,omitempty" bson:"client_id,omitempty"`
	Info        interface{}   `json:"info,omitempty" bson:"info,omitempty"`
	CreatedAt   time.Time     `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
}

// IsExists struct
func (m Audit) IsExists() (ok bool) {
	if m.ID != "" {
		ok = true
	}

	return
}
