package audit

import (
	"log"
	"sync"

	"gitlab.com/nordic-coder/skeleton/repo"
	"gitlab.ghn.vn/online/common/config"
)

// Repo type
type Repo repo.Mongo

var (
	instance *Repo
	once     sync.Once
)

// New ..
func New() *Repo {
	once.Do(func() {
		log.Println("new repo")
		instance = &Repo{
			Session:    config.GetConfig().Mongo.Get("skeleton"),
			Collection: "Audit",
		}
		log.Println("indexing")
		session, db := instance.Session.GetCollection(instance.Collection)
		db.EnsureIndexKey("identity", "type")
		session.Close()
	})
	return instance
}
