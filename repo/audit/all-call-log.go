package audit

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/nordic-coder/skeleton/model"
)

// AllCallLog func
func (r Repo) AllCallLog(orderCode string, offset int) (result []model.Audit, err error) {
	session, db := r.Session.GetCollection(r.Collection)
	err = db.Find(bson.M{"identity": orderCode, "type": "call-log"}).All(&result)
	session.Close()
	if err == mgo.ErrNotFound {
		err = nil
	}
	return
}
