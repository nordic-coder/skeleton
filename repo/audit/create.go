package audit

import (
	"time"

	"gitlab.com/nordic-coder/skeleton/model"
)

// Create func
func (r Repo) Create(data model.Audit) (err error) {
	session, db := r.Session.GetCollection(r.Collection)
	// override data
	now := time.Now()
	data.CreatedAt = now
	//
	err = db.Insert(data)
	session.Close()
	return
}
