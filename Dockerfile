FROM alpine
RUN apk add tzdata ca-certificates
ENV SOURCES /src
WORKDIR ${SOURCES}
COPY . ${SOURCES}
RUN rm -rf .git vendor
ENTRYPOINT ["/src/app"]